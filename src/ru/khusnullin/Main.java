package ru.khusnullin;

import static ru.khusnullin.CityUtils.*;

public class Main {
    public static void main(String[] args) {
        //вывод как в файле
        //print(output());

        //вывод отсортирован по названию
        //print(sortByName(output()));

        //вывод отсортирован по округу и названию
        //print(sortByNameAndDistrict(output()));

        //вывод индекса самого большого города
        //getBiggestCityId(output());

        //получение количества городов в каждом регионе
        getCitiesByRegion(output());
    }
}