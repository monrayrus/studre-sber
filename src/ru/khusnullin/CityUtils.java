package ru.khusnullin;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class CityUtils {

    public static List<City> output() {
        List<City> cities = new ArrayList<>();
        try(FileReader reader = new FileReader("resources/city_ru.csv");
            Scanner scanner = new Scanner(reader)) {
        while(scanner.hasNextLine()) {
            cities.add(from(scanner.nextLine()));
        }
    } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return (cities);
}


    public static City from(String line) {
        String[] args = line.split(";", -1);
        return new City(args[1], args[2], args[3], Integer.parseInt(args[4]), args[5]);
    }

    public static void print(List<City> list) {
        for(City city : list) {
            System.out.println(city.toString());
        }
    }

    public static List<City> sortByName(List<City> cities) {
        cities.sort(Comparator.comparing(City::getName));
        return cities;
    }

    public static List<City> sortByNameAndDistrict(List<City> cities) {
        cities.sort((c1, c2) -> {
            int result = c1.getDistrict().compareTo(c2.getDistrict());
            if (result != 0) {
                return result;
            } else {
                return c1.getName().compareTo(c2.getName());
            }
        });
        return cities;
    }


    public static void getBiggestCityId(List<City> list) {
        City[] cities = new City[list.size()];
        list.toArray(cities);
        int biggestId = 0;
        int maxPopulation = 0;
        for(int i = 0; i < cities.length; i++) {
            if(cities[i].getPopulation() > maxPopulation) {
                maxPopulation = cities[i].getPopulation();
                biggestId = i;
            }
        }
        System.out.println("[" + biggestId + "] = " + maxPopulation);
    }

    public static void getCitiesByRegion(List<City> list) {
        HashMap<String, Integer> map = new HashMap<>();
        for(City city : list) {
            if(map.containsKey(city.getRegion())) {
                map.put(city.getRegion(), map.get(city.getRegion()) + 1);
            } else {
                map.put(city.getRegion(), 1);
            }
        }
        for(String key : map.keySet()) {
            System.out.println(key + " - " + map.get(key));
        }
    }

}


