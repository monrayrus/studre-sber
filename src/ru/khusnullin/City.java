package ru.khusnullin;

public class City {

    private String name;
    private String region;
    private String district;
    private int population;
    private String foundation;

    public City(String name, String region, String district, int population, String foundation) {
        this.name = name;
        this.region = region;
        this.district = district;
        this.population = population;
        this.foundation = foundation;
    }

    public String getName() {
        return name;
    }

    public String getRegion() {
        return region;
    }

    public String getDistrict() {
        return district;
    }

    public int getPopulation() {
        return population;
    }

    public String getFoundation() {
        return foundation;
    }

    @Override
    public String toString() {
        return "City{" +
                "name='" + name + '\'' +
                ", region='" + region + '\'' +
                ", district='" + district + '\'' +
                ", population=" + population +
                ", foundation='" + foundation + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof City)) return false;

        City city = (City) o;

        if (population != city.population) return false;
        if (!name.equals(city.name)) return false;
        if (!region.equals(city.region)) return false;
        if (!district.equals(city.district)) return false;
        return foundation.equals(city.foundation);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + region.hashCode();
        result = 31 * result + district.hashCode();
        result = 31 * result + population;
        result = 31 * result + foundation.hashCode();
        return result;
    }
}
